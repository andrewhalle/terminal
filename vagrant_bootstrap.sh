#!/bin/bash

wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
/home/vagrant/Miniconda3-latest-Linux-x86_64.sh -b -p /home/vagrant/miniconda3
/home/vagrant/miniconda3/bin/pip install pyqt5 pyte
echo 'export PATH="/home/vagrant/miniconda3/bin:$PATH"' >> .bashrc
