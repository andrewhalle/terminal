import os
import sys
import termios


def main():
    termfd = sys.stdin.fileno()
    loop_cond = True
    while loop_cond:
        cmd = input("psh 0.1 > ")
        if cmd == "ls":
            print(os.listdir())
        elif cmd == "exit":
            loop_cond = False
        else:
            term_settings = termios.tcgetattr(termfd)
            pid = os.fork()
            if pid == 0:
                # child
                cmd_list = cmd.split()
                os.execvp(cmd_list[0], cmd_list)
            else:
                os.wait()
            termios.tcsetattr(termfd, termios.TCSAFLUSH, term_settings)


if __name__ == "__main__":
    main()
