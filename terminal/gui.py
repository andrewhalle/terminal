import sys
import pyte
import pty
import os
import select
import termios
import threading
from PyQt5.QtCore import Qt, QTimer, QThread
from PyQt5.QtWidgets import QApplication, QWidget, QGraphicsScene, QGraphicsView, QHBoxLayout


class WaitOnShellAndExitThread(QThread):
    def __init__(self, pid):
        super().__init__()
        self.pid = pid

    def run(self):
        os.wait()
        return

class Terminal(QWidget):
    def __init__(self):
        super().__init__()

        # start a pyte buffer and a shell thread
        self.buffer_width = 90
        self.buffer_height = 24
        self.buffer = pyte.Screen(self.buffer_width, self.buffer_height)
        self.buffer_input = pyte.ByteStream()
        self.buffer_input.attach(self.buffer)
        pid, fd = pty.fork()
        if pid == 0:
            # child, run the shell
            os.execvp("python", ["python", "psh.py"])
        self.shell_pid = pid
        self.master_tty = fd
        self.read_poll = select.poll()
        self.read_poll.register(self.master_tty, select.POLLIN)
        self.write_poll = select.poll()
        self.write_poll.register(self.master_tty, select.POLLOUT)

        # set up graphics
        self.scene = QGraphicsScene()
        self.scene.setBackgroundBrush(Qt.blue)
        self.view = QGraphicsView(self.scene)

        # set up layout
        layout = QHBoxLayout()
        layout.addWidget(self.view)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        # set up refresh timer and thread to close gui if shell exits
        self.refresh_timer = QTimer()
        self.refresh_timer.timeout.connect(self.drain_shell_to_buffer)
        self.refresh_timer.start(100)
        self.exit_thread = WaitOnShellAndExitThread(self.shell_pid)
        self.exit_thread.finished.connect(app.exit)
        self.exit_thread.start()

        # show the terminal
        self.resize(500, 300)
        self.setStyleSheet("border: 0px solid black")
        self.show()

    def keyPressEvent(self, event):
        key_typed = event.text().encode()
        self.write_poll.poll()
        os.write(self.master_tty, key_typed)
        self.refresh_screen()

    def refresh_screen(self):
        # TODO: NOT EFFICIENT, PYTE HAS A NOTION OF DIRTY LINES
        width = self.width()
        height = self.height()
        box_height = height // self.buffer_height
        self.scene = QGraphicsScene()
        self.scene.setBackgroundBrush(Qt.blue)
        self.view.setScene(self.scene)
        buffer_display = self.buffer.display
        for row in range(self.buffer_height):
            tmp = self.scene.addSimpleText(buffer_display[row])
            tmp.setPos(0, row * box_height)
        self.scene.update()

    def drain_shell_to_buffer(self):
        ready = self.read_poll.poll(10)
        if ready == []:
            return
        tmp = os.read(self.master_tty, 1024)
        if tmp != "".encode():
            self.buffer_input.feed(tmp)
        self.refresh_screen()


if __name__ == "__main__":
    app = QApplication([])
    term = Terminal()
    sys.exit(app.exec_())
